
Please see below notes about how assignment was done.

I worked on this assignment in a number of steps, had to go back to it
a few times. I hope this is suitable. I have added how much time approximately I've
spent each time.


The app consists of single screen, which contains 2 Edit texts - one to enter venue name,
another one is to enter location. If location is not entered, it will use hardcoded coordinates
in the request to find nearby venues(the idea is to use device location in this case).
If area is entered, it will use this area to search 'near'.
Venue name is also optional, so if it is empty, all nearby venues will be searched for.

With regards to development approach:
	- I have never used any of the libraries mentioned (Dagger 2, RX Java, Retrofit)
	- I have used tutorial mentioned below, which pretty much reflects requirements, apart from RX Java.
	  I adapted it to use Foursquare API and relevant models as well with a few changes to the UI.
	- Once this worked, I have used another tutorial to add RX Java library
	  (https://medium.com/@nurrohman/a-simple-android-apps-with-mvp-dagger-rxjava-and-retrofit-4edb214a66d7)

Here are the steps:

1. 11 July - Time spent: around 2 hours
Did some research about Retrofit and Dagger2 injection,
as I've never used them before. Found and completed this great tutorial online,
which contains both: https://www.raywenderlich.com/146804/dependency-injection-dagger-2

Started project development, used hardcoded values for query at start,
formed similar structure for the project as in tutorial provided.
It seems to be working, but there is a problem with Gson serialization,
because the same URL works when I test it on foursquare page and returns valid results


2. 11 July - Time spent: 2 hours
Fixed problem with serailization, can get list of results
and display them in RecyclerView. Added support to search venues by name,
depending on what is entered in textbox. Added new field to the ViewHolder
to show the distance (even though location is still hardcoded)

3. 12 July - Time spent: 1 hour
Added another field to allow user to enter location,
so venue names can be searched either in this location,
or (if empty) in hardcoded London coordinates. Few formatting changes.

4. 12 July - Time spent: 1 hour
Added Dagger 2 dependency injection. I followed tutorial fully,
as I don't have any experience in working with injections. In our current project the decision
was made to not do injections, because it could create issues with tracing, finding usages.
Also, general opinion was that it is difficult to do a correct configuration for dependency injection.

5. 13 July - Time spent: 1.5 hours
Added RX Java. Followed this tutorial:
https://medium.com/@nurrohman/a-simple-android-apps-with-mvp-dagger-rxjava-and-retrofit-4edb214a66d7
and adapted it for the project. Added another label to display
postcode of the venue (even though realised american one is received frequently :-))
Added comparator to order venues list by distance where it is available (from nearest to furthest)


I hope this is clear enough, however if you have some questions, I'll be happy to answer them.

Best wishes,
Elena



Sources and helpers used:
1. https://www.raywenderlich.com/146804/dependency-injection-dagger-2
2. http://pojo.sodhanalibrary.com/
3. https://jsonformatter.org/
4. http://devnextdoor.com/2016/11/29/using-retrofit-with-jackson-converter-in-android/
5. https://medium.com/@nurrohman/a-simple-android-apps-with-mvp-dagger-rxjava-and-retrofit-4edb214a66d7


