package com.dev.etishkova.foursquarewhit.network;

import com.dev.etishkova.foursquarewhit.helpers.Constants;
import com.dev.etishkova.foursquarewhit.models.PostResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by elenatishkova on 11/07/2017.
 */

public interface FoursquareApi {

    /**
     * It is required to have either "near"  or "location" parameters
     * <p></p>
     * @param name optional parameter, if null, will not be added to query
     * @param location optional, if near is present
     * @param nearArea optional if location is present
     * @return
     */

    @GET(Constants.SEARCH_REQUEST + "&client_id=" + Constants.CLIENT_ID + "&client_secret=" + Constants.CLIENT_SECRET)
    Observable<PostResponse> getVenuesList(@Query("query") String name, @Query("ll") String location, @Query("near") String nearArea);
}
