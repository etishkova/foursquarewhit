package com.dev.etishkova.foursquarewhit.ui.places;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dev.etishkova.foursquarewhit.R;
import com.dev.etishkova.foursquarewhit.helpers.Constants;
import com.dev.etishkova.foursquarewhit.app.VenuesApplication;
import com.dev.etishkova.foursquarewhit.models.Venues;

import java.util.List;

import javax.inject.Inject;

import static android.view.View.GONE;

public class VenuesActivity extends AppCompatActivity implements IVenuesView, View.OnClickListener {

    private EditText etPlaceNameEntry;
    private EditText etLocationEntry;
    private ProgressBar pbProgressBar;
    private Button btnSearch;
    private RecyclerView rvPlacesResult;
    private AlertDialog alertDialog;

    @Inject
    IVenuesPresenter venuesPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);

        ((VenuesApplication) getApplication()).getAppComponent().inject(this);
        findAndSetupViews();

        venuesPresenter.setView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void findAndSetupViews() {
        etPlaceNameEntry = (EditText) findViewById(R.id.etPlaceNameEntry);
        etLocationEntry = (EditText) findViewById(R.id.etLocationEntry);
        pbProgressBar = (ProgressBar) findViewById(R.id.pbProgressBar);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        rvPlacesResult = (RecyclerView) findViewById(R.id.rvPlacesResult);
        btnSearch.setOnClickListener(this);
        rvPlacesResult.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        hideLoading();

        alertDialog = new AlertDialog.Builder(VenuesActivity.this).create();
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
    }

    @Override
    public void showLoading() {
        pbProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        pbProgressBar.setVisibility(GONE);
    }

    @Override
    public void showVenues(List<Venues> venuesItemList) {
        hideLoading();
        rvPlacesResult.setAdapter(new VenuesAdapter(venuesItemList));
        rvPlacesResult.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void showErrorMessage() {
        hideLoading();
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Error processing your request. Please try again later");
        alertDialog.show();
    }

    @Override
    public void showNoVenuesReturned() {
        hideLoading();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Could not find any venues matching your request");
        alertDialog.show();
    }

    @Override
    public void launchVenueDetail(Venues venue) {
        //TODO: open another activity with venue details
        Toast.makeText(this, "Show venue details " + venue.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        if (v == btnSearch) {
            rvPlacesResult.smoothScrollToPosition(0);
            if (rvPlacesResult.getAdapter() != null)
                ((VenuesAdapter) rvPlacesResult.getAdapter()).clearData();
            venuesPresenter.getVenues(etPlaceNameEntry.getText().toString(), getLat(), getLon(), etLocationEntry.getText().toString());
        }
    }

    private Double getLat() {
        return Constants.MOCKED_DEVICE_LAT;
    }

    private Double getLon() {
        return Constants.MOCKED_DEVICE_LON;
    }

    private class VenuesAdapter extends RecyclerView.Adapter<VenuesViewHolder> {

        private List<Venues> venuesList;

        VenuesAdapter(List<Venues> venuesList) {
            this.venuesList = venuesList;
        }

        @Override
        public VenuesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(VenuesActivity.this);
            return new VenuesViewHolder(inflater.inflate(R.layout.list_item_place, parent, false));
        }

        @Override
        public void onBindViewHolder(VenuesViewHolder holder, int position) {
            final Venues venueItem = venuesList.get(position);
            holder.setValues(venueItem);
            holder.getContainer().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    launchVenueDetail(venueItem);
                }
            });
        }

        @Override
        public int getItemCount() {
            return venuesList.size();
        }

        public void clearData() {
            venuesList.clear();
            notifyDataSetChanged();
        }
    }
}
