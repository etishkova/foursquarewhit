package com.dev.etishkova.foursquarewhit.app;

import android.app.Application;

import com.dev.etishkova.foursquarewhit.dagger.AppComponent;
import com.dev.etishkova.foursquarewhit.dagger.AppModule;
import com.dev.etishkova.foursquarewhit.dagger.DaggerAppComponent;

/**
 * Created by elenatishkova on 11/07/2017.
 */

public class VenuesApplication extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = initDagger(this);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    protected AppComponent initDagger(VenuesApplication application) {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(application))
                .build();
    }
}
