package com.dev.etishkova.foursquarewhit.ui.places;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dev.etishkova.foursquarewhit.R;
import com.dev.etishkova.foursquarewhit.helpers.ConversionHelper;
import com.dev.etishkova.foursquarewhit.models.Venues;

import static android.view.View.GONE;

/**
 * Created by elenatishkova on 11/07/2017.
 */

public class VenuesViewHolder extends RecyclerView.ViewHolder {

    private ViewGroup llPlacesContainer;
    private TextView tvPlaceName;
    private TextView tvDistance;
    private TextView tvPostcode;

    public VenuesViewHolder(View view) {
        super(view);
        llPlacesContainer = (LinearLayout) view.findViewById(R.id.llPlacesContainer);
        tvPlaceName = (TextView) view.findViewById(R.id.tvPlaceName);
        tvDistance = (TextView) view.findViewById(R.id.tvDistance);
        tvPostcode = (TextView) view.findViewById(R.id.tvPostcode);
    }

    public ViewGroup getContainer() {
        return llPlacesContainer;
    }

    public void setValues(Venues venue){
        tvPlaceName.setText(venue.getName());
        setDistanceLabel(venue);
        setPostcodeLabel(venue);
    }

    private void setPostcodeLabel(Venues venue){
        if (venue.getLocation() != null){
            String postcode = venue.getLocation().getPostalCode();
            if (postcode != null && !postcode.trim().isEmpty()){
                tvPostcode.setText(postcode);
                return;
            }
        }
        tvDistance.setVisibility(GONE);
    }

    private void setDistanceLabel(Venues venue) {
        if (venue.getLocation() != null){
            String distance = venue.getLocation().getDistance();
            if (distance != null && !distance.trim().isEmpty()){
                String milesDistance = ConversionHelper.getMilesFromStringValue(venue.getLocation().getDistance());
                if (milesDistance != null && !milesDistance.isEmpty()) {
                    tvDistance.setText(milesDistance + " miles away");
                } else {
                    tvDistance.setText("Could not process distance");
                }
                return;
            }
        }
        tvDistance.setVisibility(GONE);
    }
}
