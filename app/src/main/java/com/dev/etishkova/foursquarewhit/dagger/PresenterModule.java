package com.dev.etishkova.foursquarewhit.dagger;

import android.content.Context;

import com.dev.etishkova.foursquarewhit.rxjava.Service;
import com.dev.etishkova.foursquarewhit.ui.places.IVenuesPresenter;
import com.dev.etishkova.foursquarewhit.ui.places.VenuesPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by elenatishkova on 12/07/2017.
 */

@Module
public class PresenterModule {

    @Provides
    @Singleton
    IVenuesPresenter provideVenuesPresenter(Context context) {
        return new VenuesPresenter(context);
    }
}
