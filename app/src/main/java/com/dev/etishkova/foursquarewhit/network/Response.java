package com.dev.etishkova.foursquarewhit.network;

import com.google.gson.annotations.SerializedName;

/**
 * Created by elenatishkova on 13/07/2017.
 */

public class Response {
    @SerializedName("status")
    public String status;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @SuppressWarnings({"unused", "used by Retrofit"})
    public Response() {
    }

    public Response(String status) {
        this.status = status;
    }
}
