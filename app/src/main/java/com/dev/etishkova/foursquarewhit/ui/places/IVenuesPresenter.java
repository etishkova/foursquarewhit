package com.dev.etishkova.foursquarewhit.ui.places;

import android.support.annotation.Nullable;

/**
 * Created by elenatishkova on 11/07/2017.
 */

public interface IVenuesPresenter {

    void setView(IVenuesView view);

    void getVenues(String venueName, @Nullable Double lat, @Nullable Double lon, @Nullable String near);
}
