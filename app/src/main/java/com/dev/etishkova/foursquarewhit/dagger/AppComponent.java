package com.dev.etishkova.foursquarewhit.dagger;

import com.dev.etishkova.foursquarewhit.ui.places.VenuesActivity;
import com.dev.etishkova.foursquarewhit.ui.places.VenuesPresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by elenatishkova on 12/07/2017.
 */
@Singleton
@Component(modules = {AppModule.class, PresenterModule.class, NetworkModule.class})
public interface AppComponent {

    void inject(VenuesActivity target);

    void inject(VenuesPresenter target);
}
