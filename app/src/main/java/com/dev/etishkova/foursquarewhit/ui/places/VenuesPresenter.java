package com.dev.etishkova.foursquarewhit.ui.places;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.dev.etishkova.foursquarewhit.helpers.ConversionHelper;
import com.dev.etishkova.foursquarewhit.app.VenuesApplication;
import com.dev.etishkova.foursquarewhit.helpers.VenueComparator;
import com.dev.etishkova.foursquarewhit.models.PostResponse;
import com.dev.etishkova.foursquarewhit.models.Venues;
import com.dev.etishkova.foursquarewhit.rxjava.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by elenatishkova on 11/07/2017.
 */

public class VenuesPresenter implements IVenuesPresenter {


    private IVenuesView view;

    private String venueNameToSearchFor;
    private Double lat;
    private Double lon;
    private String near;

    /*@Inject
    FoursquareApi fourSquareApi;*/

    //private final Service service;

    @Inject
    public  Service service;

    private CompositeSubscription subscriptions;

    public VenuesPresenter(Context context) {

        ((VenuesApplication) context).getAppComponent().inject(this);
        this.subscriptions = new CompositeSubscription();
    }

    @Override
    public void setView(IVenuesView view) {
        this.view = view;
    }

    @Override
    public void getVenues(@Nullable String venueName, @Nullable Double lat, @Nullable Double lon, @Nullable String near) {
        view.showLoading();

        this.venueNameToSearchFor = venueName;
        this.lon = lon;
        this.lat = lat;
        this.near = near;
        processValuesForRequest();


        Service.GetVenuesListCallback callback = new Service.GetVenuesListCallback(){
            @Override
            public void onSuccess(PostResponse postResponse) {
                if (postResponse == null || postResponse.getResponse().getVenues() == null || postResponse.getResponse().getVenues().length == 0) {
                    view.showNoVenuesReturned();
                    return;
                }
                Venues[] venues = postResponse.getResponse().getVenues();
                if (venues != null && venues.length != 0) {
                    ArrayList<Venues> venuesList = new ArrayList<>(Arrays.asList(venues));
                    Collections.sort(venuesList, new VenueComparator());
                    view.showVenues(venuesList);
                }
            }

            @Override
            public void onError() {
                view.showErrorMessage();
            }
        };

        Subscription subscription = service.getVenuesList(callback, venueNameToSearchFor, ConversionHelper.convertLatLonToProcessableString(this.lat, this.lon), this.near);

        subscriptions.add(subscription);


        /*fourSquareApi.getVenuesList(venueNameToSearchFor, ConversionHelper.convertLatLonToProcessableString(this.lat, this.lon), this.near).enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                if (response.code() != 200) {
                    view.showErrorMessage();
                } else {
                    final PostResponse postResponse = response.body();
                    if (postResponse == null || postResponse.getResponse().getVenues() == null || postResponse.getResponse().getVenues().length == 0) {
                        view.showNoVenuesReturned();
                        return;
                    }
                    Venues[] venues = postResponse.getResponse().getVenues();
                    if (venues != null && venues.length != 0) {
                        ArrayList<Venues> venuesList = new ArrayList<Venues>(Arrays.asList(venues));
                        view.showVenues(venuesList);
                    }

                }
                view.hideLoading();
            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                view.showErrorMessage();
                view.hideLoading();
            }
        });*/
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }

    private void processValuesForRequest() {
        if (near != null && !near.trim().isEmpty()) {       //if near is available, no need to provide coordinates, they will be null
            lat = null;
            lon = null;
        } else near = null;
        if (venueNameToSearchFor != null && venueNameToSearchFor.trim().isEmpty())    //if no value entered or only spaces, pass null
            venueNameToSearchFor = null;
    }
}
