package com.dev.etishkova.foursquarewhit.helpers;

import android.support.annotation.Nullable;

/**
 * Created by elenatishkova on 12/07/2017.
 */

public class ConversionHelper {


    public static String getMilesFromStringValue(@Nullable String stringToConvert) {
        if (stringToConvert != null && !stringToConvert.isEmpty()) {
            try {
                Double converted = Double.valueOf(stringToConvert);
                converted = converted * 0.000621371192;
                return String.format("%.2f", converted);
            } catch (NumberFormatException ex) {
                return stringToConvert;
            }
        }
        return stringToConvert;
    }

    public static String convertLatLonToProcessableString(Double lat, Double lon) {
        if (lat != null && lon != null) {
            return lat + "," + lon;
        } else return null;
    }
}
