package com.dev.etishkova.foursquarewhit.ui.places;

import com.dev.etishkova.foursquarewhit.models.Venues;

import java.util.List;

/**
 * Created by elenatishkova on 11/07/2017.
 */

public interface IVenuesView {

    void showLoading();

    void hideLoading();

    void showVenues(List<Venues> venuesItemList);

    void showErrorMessage();

    void showNoVenuesReturned();

    void launchVenueDetail(Venues venue);
}
