package com.dev.etishkova.foursquarewhit.helpers;

import com.dev.etishkova.foursquarewhit.models.Venues;

import java.util.Comparator;

/**
 * Created by elenatishkova on 13/07/2017.
 */

public class VenueComparator implements Comparator<Venues> {

    @Override
    public int compare(Venues v1, Venues v2) {
        if (v1.getLocation() != null && v2.getLocation() != null) {
            String distance1 = v1.getLocation().getDistance();
            String distance2 = v2.getLocation().getDistance();

            if (distance1 != null && distance2 != null) {
                try {
                    Double distanceInMeters1 = Double.valueOf(distance1);
                    Double distanceInMeters2 = Double.valueOf(distance2);
                    return distanceInMeters1.compareTo(distanceInMeters2);
                } catch (NumberFormatException ex){
                    //TODO: handle exception, log
                    return 0;
                }
            }
        }
        return 0;
    }
}
