package com.dev.etishkova.foursquarewhit.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by elenatishkova on 11/07/2017.
 */

public class Contact {

    private String twitter;

    private String phone;

    private String formattedPhone;

    public String getTwitter ()
    {
        return twitter;
    }

    public void setTwitter (String twitter)
    {
        this.twitter = twitter;
    }

    public String getPhone ()
    {
        return phone;
    }

    public void setPhone (String phone)
    {
        this.phone = phone;
    }

    public String getFormattedPhone ()
    {
        return formattedPhone;
    }

    public void setFormattedPhone (String formattedPhone)
    {
        this.formattedPhone = formattedPhone;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [twitter = "+twitter+", phone = "+phone+", formattedPhone = "+formattedPhone+"]";
    }
}
