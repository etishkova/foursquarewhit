package com.dev.etishkova.foursquarewhit.rxjava;

import com.dev.etishkova.foursquarewhit.models.PostResponse;
import com.dev.etishkova.foursquarewhit.network.FoursquareApi;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by elenatishkova on 13/07/2017.
 */

public class Service {

    public interface GetVenuesListCallback {
        void onSuccess(PostResponse response);

        void onError();
    }

    private final FoursquareApi networkService;

    public Service(FoursquareApi networkService) {
        this.networkService = networkService;
    }

    public Subscription getVenuesList(final GetVenuesListCallback callback, String name, String location, String nearArea) {
        return networkService.getVenuesList(name, location, nearArea)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends PostResponse>>() {

                    @Override
                    public Observable<? extends PostResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<PostResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError();
                        unsubscribe();
                    }

                    @Override
                    public void onNext(PostResponse response) {
                        callback.onSuccess(response);
                    }
                });
    }
}
